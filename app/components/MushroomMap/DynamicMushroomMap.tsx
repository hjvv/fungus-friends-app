import dynamic from "next/dynamic";

const DynamicMushroomMap = dynamic(() => import("./MushroomMap"), {
  ssr: false,
});

export default DynamicMushroomMap;
